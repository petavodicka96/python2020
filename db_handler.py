import psycopg2
import requests
import geojson
from multiprocessing import Process
import datetime

def dbConnect(db, host, user, password):
    """
    Connects the specified PostgreSQL database
    :param db: database name (str)
    :param host: host IP (str)
    :param user: user name (str)
    :param password: password (str)
    :return: a connection object
    """
    try:
        conn = psycopg2.connect(f"dbname={db} user={user} host={host} password={password}")
    except psycopg2.OperationalError:   # exception to raise if DB not exist
        print("DB koronavirus not found. Creating the database...")
        conn = dbCreate(db, host, user, password)

    print("Database connected.")
    return conn


def dbCreate(db, host, user, password):
    """
    Creates a new DB with all tables
    :param db: database name (str)
    :param host: host IP (str)
    :param user: user name (str)
    :param password: password (str)
    :return: a connection object
    """
    conn = psycopg2.connect(f"user={user} host={host} password={password}") # connect DB server
    conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
    cursor = conn.cursor()  # open DB cursor
    cursor.execute("CREATE DATABASE {};".format(db))    # create the DB

    conn = psycopg2.connect(f"dbname={db} user={user} host={host} password={password}")  # connect the DB
    cursor = conn.cursor()  # open DB cursor
    cursor.execute("CREATE EXTENSION postgis;")  # create postgis extension
    # create all the tables
    cursor.execute("""CREATE TABLE data_obce (
                                datum DATE,
                                kod_obec CHAR(6),
                                nove_pripady INTEGER,
                                aktualne_nemocni INTEGER,
                                PRIMARY KEY (kod_obec, datum));
                            """)
    cursor.execute("""CREATE TABLE testy (
                                datum DATE PRIMARY KEY,
                                testy_prirustek INTEGER,
                                testy_kumul INTEGER,
                                prvni_testy_prirustek INTEGER,
                                prvni_testy_kumul INTEGER);
                            """)
    cursor.execute("""CREATE TABLE nakazeni (
                                datum DATE PRIMARY KEY,
                                nakazeni_prirustek INTEGER,
                                nakazeni_kumul INTEGER);
                            """)
    cursor.execute("""CREATE TABLE souhrn (
                                datum DATE PRIMARY KEY,
                                vyleceni INTEGER,
                                zemreli INTEGER);
                            """)
    cursor.execute("""CREATE TABLE obce_geom (
                                kod_obec CHAR(6) PRIMARY KEY,
                                naz_obec VARCHAR,
                                kod_orp CHAR(4),
                                kod_lau1 CHAR(6),
                                kod_nuts3 CHAR(5),
                                pocet_ob INTEGER,
                                muzi INTEGER,
                                zeny INTEGER,
                                ob_0_14 INTEGER,
                                ob_15_64 INTEGER,
                                ob_65 INTEGER,
                                nezam REAL,
                                geom GEOMETRY);
                            """)

    processes = []
    for i in range(1, 5):   # create 4 processes to insert spatial data from GEOJSON located at gitlab
        url = f"https://gitlab.com/petavodicka96/python2020/-/raw/master/obce{i}.geojson"
        process = Process(target=insertSpatialData, args=(db, host, user, password, url))
        processes.append(process)
        process.start() # run the process
    for process in processes:
        process.join()  # wait until all processes run

    conn.commit()   # make changes in the DB persistent
    print("Database created.")
    return conn


def insertSpatialData(db, host, user, password, url):
    """
    Downloads GEOJSON spatial data of czech municipalities and inserts it to the DB
    :param db: database name (str)
    :param host: host IP (str)
    :param user: user name (str)
    :param password: password (str)
    :param url: GEOJSON url (str)
    """
    conn = dbConnect(db, user, host, password)  # connect the DB
    cursor = conn.cursor()  # open DB cursor
    download = requests.get(url).text   # load GEOJSON data as a plain text
    gj = geojson.loads(download)    # manage it as GEOJSON
    for feature in gj["features"]:  # insert each feature (municipality) in the DB
        geom = geojson.dumps(feature["geometry"])
        kod_obec = feature["properties"]["KOD_OBEC"]
        naz_obec = feature["properties"]["NAZ_OBEC"]
        kod_orp = feature["properties"]["KOD_ORP"]
        kod_lau1 = feature["properties"]["KOD_LAU1"]
        kod_nuts3 = feature["properties"]["KOD_CZNUTS3"]
        pocet_ob = int(feature["properties"]["POCET_OBYV"]) if feature["properties"]["POCET_OBYV"] is not None else None
        muzi = int(feature["properties"]["MUZI"]) if feature["properties"]["MUZI"] is not None else None
        zeny = int(feature["properties"]["ZENY"]) if feature["properties"]["ZENY"] is not None else None
        ob_0_14 = int(feature["properties"]["OBYV_0_14"]) if feature["properties"]["OBYV_0_14"] is not None else None
        ob_15_64 = int(feature["properties"]["OBYV_15_64"]) if feature["properties"]["OBYV_15_64"] is not None else None
        ob_65 = int(feature["properties"]["OBYV_65"]) if feature["properties"]["OBYV_65"] is not None else None
        nezam = float(feature["properties"]["MIRA_NEZAM"]) if feature["properties"]["MIRA_NEZAM"] is not None else None
        cursor.execute(
        "INSERT INTO obce_geom VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s, St_SetSRID(ST_GeomFromGeoJSON(%s),32633))",
        (kod_obec, naz_obec, kod_orp, kod_lau1, kod_nuts3, pocet_ob, muzi, zeny, ob_0_14, ob_15_64, ob_65, nezam, geom))


def insertData(table, columns, name, db, host, user, password):
    """
    Inserts data downloaded from the COVID server into DB
    :param table: selected rows of the original CSV to insert into the DB (str)
    :param columns: selected columns of the original CSV to insert into the DB (list of ints)
    :param name: database table name (str)
    :param db: database name (str)
    :param host: host IP (str)
    :param user: user name (str)
    :param password: password (str)
    """
    conn = dbConnect(db, user, host, password)  # connect the DB
    cursor = conn.cursor()  # open the DB cursor
    for line in table:  # for each CSV entry
        values = line.split(",")    # split the values into a list
        try:
            # select the specified columns and convert it to the desired data type
            # first selected column is always date and the others are integers
            cols = [datetime.datetime.strptime(values[col], "%Y-%m-%d").date() if i == 0 else (
                int(values[col]) if values[col] != "" else None) for i, col in enumerate(columns)]
            sql = "INSERT INTO " + name + " VALUES (" + ("%s, " * len(columns))[:-2] + ")"  # build insert statement
            cursor.execute(sql, tuple(cols))
        except: # deals with header and empty rows
            pass
    conn.commit()   # make changes in the DB persistent


def dbUpdate(db, host, user, password):
    """
    Loads data from COVID server and updates the specified PostgreSQL database
    :param db: database name (str)
    :param host: host IP (str)
    :param user: user name (str)
    :param password: password (str)
    :return: a connection object
    """
    conn = dbConnect(db, host, user, password)  # connect the DB
    cursor = conn.cursor()  # open the DB cursor
    print("Loading data from COVID server...")

    # stores data from the COVID server and some auxiliary values
    tables = [{"name": "data_obce", "data": requests.get("https://onemocneni-aktualne.mzcr.cz/api/v2/covid-19/obce.csv").text.split("\r\n"), "columns": [1, 8, 10, 11]},
              {"name": "testy", "data": requests.get("https://onemocneni-aktualne.mzcr.cz/api/v2/covid-19/testy.csv").text.split("\r\n"), "columns": [0, 1, 2, 3, 4]},
              {"name": "nakazeni", "data": requests.get("https://onemocneni-aktualne.mzcr.cz/api/v2/covid-19/nakaza.csv").text.split("\r\n"), "columns": [0, 1, 2]},
              {"name": "souhrn", "data": requests.get("https://onemocneni-aktualne.mzcr.cz/api/v2/covid-19/nakazeni-vyleceni-umrti-testy.csv").text.split("\r\n"), "columns": [0, 2, 3]}]

    print("Data loaded. Updating the DB...")

    for table in tables:    # for each table of the DB
        #  first delete all existing entries
        delete_string = "DELETE FROM " + table["name"]
        cursor.execute(delete_string)
        conn.commit()

        num_rows = len(table["data"]) + 1   # count entries in the new CSV loaded from COVID server
        processes = []
        for i in range(4):  # create 4 processes to insert the data into DB
            process = Process(target=insertData, args=(
            table["data"][int(num_rows / 4 * i):int(num_rows / 4 * (i + 1))],
            table["columns"], table["name"], db, host, user, password))
            processes.append(process)
            process.start()
        for process in processes:
            process.join()  # wait until all processes run

        print("Table " + table["name"] + " updated.")

    return conn
