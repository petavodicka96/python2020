from db_handler import *
from plots import *

# funkcionality DEMO
if __name__ == '__main__':

    # DB update
    conn = dbUpdate("koronavirus", "localhost", "postgres", "postgres")

    # time series of tests performed from beginning of the epidemia til January 24, 2021
    fig, ax = plt.subplots()
    performedTests(ax, conn, "2021-01-24", color="yellow")
    plt.show()
    # export as PNG picture
    fig.savefig("tests.png", dpi=600)

    # time series of tests performed from December 24, 2020 til January 24, 2021
    fig, ax = plt.subplots()
    performedTests(ax, conn, "2021-01-24", "2020-12-24", color="yellow")
    plt.show()

    # time series of cummulative tests performed from beginning of the epidemia til January 24, 2021
    fig, ax = plt.subplots()
    performedTests(ax, conn, "2021-01-24", cummulative=True, color="yellow")
    plt.show()

    # functionality of infectedPeople, diedPeople and healedPeople funcs is analogous

    # prevalention map of the Czech Republic at January 24, 2021
    fig, ax = plt.subplots()
    plotPrevalentionMap(ax, conn, "2021-01-24")
    plt.show()

    # prevalention map of Southmoravian region at January 24, 2021
    fig, ax = plt.subplots()
    plotPrevalentionMap(ax, conn, "2021-01-24", region="CZ064")
    plt.show()

    # map of change in prevalention in Southmoravian region between December 24, 2020 and January 24, 2021
    fig, ax = plt.subplots()
    plotChangeMap(ax, conn, "2021-01-24", "2020-12-24", region="CZ064")
    plt.show()

    # save composition with maps of Southmoravian region
    # and time series of epidemia evolution between December 24, 2020 and January 24, 2021
    plotComposition(conn, "2021-01-24", "2020-12-24", False, region="CZ063")

    # close DB connection
    conn.close()
