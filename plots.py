import geopandas as gpd
import matplotlib.pyplot as plt
from matplotlib.dates import DateFormatter, DayLocator
from matplotlib.gridspec import GridSpec
from mpl_toolkits.axes_grid1 import make_axes_locatable
import datetime

def plotPrevalentionMap(ax, conn, date, region=None):
    """
    Plots a map of covid prevalention rate at specified date and region
    :param ax: a subplot for plotting the map into (AxesSubplot object)
    :param conn: connection to the PostgreSQL DB (connection object)
    :param date: date at which the prevalention should be calculated and ploted (str in fmt YYYY-MM-DD)
    :param region: NUTS3 code of the desired region (str), default is None, i.e. plotting whole Czech republic
    """
    reg = "(SELECT * FROM obce_geom WHERE kod_nuts3 = '{}')".format(region) if region else "obce_geom"
    sql = f"""SELECT naz_obec, aktualne_nemocni, 1000*aktualne_nemocni/pocet_ob AS prevalence, geom 
            FROM {reg} AS g
            LEFT JOIN (SELECT * FROM data_obce WHERE datum = '{date}') AS d 
            ON (g.kod_obec = d.kod_obec);"""

    print("Loading data from DB to draw the prevalention map...")
    polyg = gpd.read_postgis(sql, conn)         # read spatial data from DB
    print("Data loaded. Drawing...")

    vmax = max(polyg.prevalence)    # upper bound of the legend

    ax.set_axis_off()  # set frame and coordinates not visible
    ax.set_title(f"Počet nakažených na 1 000 ob. v obcích ČR\ndne {date[8:]}. {date[5:7]}. {date[:4]}")  # set map title
    polyg.plot(column="prevalence", ax=ax, vmin=0, vmax=vmax, cmap='Reds', edgecolor='black', linewidth=0.05)  # plot it
    divider = make_axes_locatable(ax)       # make the subplot divisible
    cax = divider.append_axes("right", size=0.1, pad=0.1)   # append an extra subplot for colorbar
    n_cmap = plt.cm.ScalarMappable(cmap='Reds', norm=plt.Normalize(vmin=0, vmax=vmax)) # set colors of the bar
    ax.figure.colorbar(n_cmap, cax=cax)   # add the colorbar to the plot
    print("Prevalention map finished.")


def plotChangeMap(ax, conn, end, start="2020-03-01", region=None):
    """
    Plots a map of change in covid prevalention rate between two specified dates and region
    :param ax: a subplot for plotting the map into (AxesSubplot object)
    :param conn: connection to the PostgreSQL DB (connection object)
    :param end: final date to calculate and plot the change in covid prevalention (str in fmt YYYY-MM-DD)
    :param start: reference date (str in fmt YYYY-MM-DD), default 2020-03-01, i.e. first covid case in the Czech Rep.
    :param region: NUTS3 code of the desired region (str), default is None, i.e. plotting whole Czech republic
    """
    reg = "(SELECT * FROM obce_geom WHERE kod_nuts3 = '{}')".format(region) if region else "obce_geom"
    sql = f"""SELECT naz_obec, 1000*(aktualne_nemocni-predtim_nemocni)/cast(pocet_ob as decimal) AS rel_zmena, geom 
            FROM {reg} AS g
            LEFT JOIN ((SELECT kod_obec, aktualne_nemocni AS predtim_nemocni FROM data_obce WHERE datum = '{start}') AS d1
                INNER JOIN (SELECT kod_obec as kod, aktualne_nemocni FROM data_obce WHERE datum = '{end}') AS d2
                ON (kod_obec = kod)) AS d
            ON (g.kod_obec = d.kod_obec);"""

    print("Loading data from DB to draw the prevalention change map...")
    polyg = gpd.read_postgis(sql, conn)         # read spatial data from DB
    print("Data loaded. Drawing...")

    bound = max((abs(min(polyg.rel_zmena))), (abs(max(polyg.rel_zmena))))  # max dev from 0 (to make legend symetric)

    ax.set_axis_off()       # set frame and coordinates not visible
    ax.set_title(f"Změna počtu nakažených COVID-19 na 1 000 ob. v obcích ČR\nmezi "
                 f"dny {start[8:]}. {start[5:7]}. {start[:4]} a {end[8:]}. {end[5:7]}. {end[:4]}")  # set map title
    polyg.plot(column="rel_zmena", ax=ax, vmin=-1*bound, vmax=bound, cmap='RdBu_r', edgecolor='black', linewidth=0.05)    # plot the data
    divider = make_axes_locatable(ax)       # make the subplot divisible
    cax = divider.append_axes("right", size=0.1, pad=0.1)   # append an extra subplot for colorbar
    n_cmap = plt.cm.ScalarMappable(cmap='RdBu_r', norm=plt.Normalize(vmin=-1*bound, vmax=bound))  # set the colorbar
    ax.figure.colorbar(n_cmap, cax=cax)   # add the colorbar to the plot
    print("Prevalention change map finished.")


def plotTimeSeries(func):
    """
    Decorator function for plotting time series of covid data
    :param func: decorated function
    :return: wrapper function
    """
    def wrapper(ax, conn, end, start="2020-03-01", cummulative=False, color="tomato"):
        sql, y_label = func(ax, conn, end, start, cummulative, color)  # load return values of decorated function
        print("Loading data from DB to plot the time series...")
        cursor = conn.cursor()      # open DB cursor
        cursor.execute(sql)     # execute the sql
        x, y = [list(i) for i in zip(*cursor.fetchall())]   # save sql output into two lists for x and y
        cursor.close()      # close DB cursor
        print("Data loaded. Plotting the time series...")

        # calcutlate 7-days rolling means of y
        rolling_mean = [sum(subarray) / len(subarray) for subarray in [(y[max(0, i - 6):i + 1]) for i in range(len(y))]]

        ax.plot(x, y, label="Originální data", color=color)     # plot the original data
        ax.xaxis.set_major_formatter(DateFormatter("%d.%m.%y"))     # set date format
        # calculate time period between start and end
        num_days = (datetime.datetime.strptime(end, "%Y-%m-%d") - datetime.datetime.strptime(start, "%Y-%m-%d")).days
        ax.xaxis.set_major_locator(DayLocator(interval=(num_days // 6)))    # calculate position of x label ticks
        ax.ticklabel_format(useOffset=False, style="plain", axis="y")     # prevent scientific notation
        ax.yaxis.set_label_coords(-0.128, 0.5)        # align y labels
        ax.set_xlabel("Datum")
        ax.set_ylabel(y_label)
        ax.tick_params(axis="both", labelsize=6)        # set label size
        ax.plot(x, rolling_mean, label="7denní klouzavé průměry", linewidth=1, color="black")   # plot rolling means
        ax.legend(loc="best")       # plot the legend
        print("Time series plotted.")

    return wrapper

@plotTimeSeries
def infectedPeople(ax,conn,end,start="2020-03-01", cummulative=False, color="tomato"):
    """
    Plots the number of infected people against date for specified period
    :param ax: a subplot for plotting the time series into (AxesSubplot object)
    :param conn: connection to the PostgreSQL DB (connection object)
    :param end: last date to plot (str in fmt YYYY-MM-DD)
    :param start: first date to plot (str in fmt YYYY-MM-DD), default 2020-03-01, i.e. first covid case in CZE
    :param cummulative: flag if cummulative values should be plotted (bool), default False
    :param color: line color in the plot (str), default "tomato"
    :return: sql statement and default y label (tuple of two strings)
    """
    if not cummulative:
        sql = f"SELECT datum,nakazeni_prirustek FROM nakazeni WHERE datum BETWEEN '{start}' AND '{end}' ORDER BY datum"
    else:
        sql = f"SELECT datum,nakazeni_kumul FROM nakazeni WHERE datum BETWEEN '{start}' AND '{end}' ORDER BY datum"
    return sql, "Počet nakažených"


@plotTimeSeries
def performedTests(ax, conn, end, start="2020-03-01", cummulative=False, color="tomato"):
    """
    Plots the number of performed tests against date for specified period
    :param ax: a subplot for plotting the time series into (AxesSubplot object)
    :param conn: connection to the PostgreSQL DB (connection object)
    :param end: last date to plot (str in fmt YYYY-MM-DD)
    :param start: first date to plot (str in fmt YYYY-MM-DD), default 2020-03-01, i.e. first covid case in CZE
    :param cummulative: flag if cummulative values should be plotted (bool), default False
    :param color: line color in the plot (str), default "tomato"
    :return: sql statement and default y label (tuple of two strings)
    """
    if not cummulative:
        sql = f"SELECT datum,testy_prirustek FROM testy WHERE datum BETWEEN '{start}' AND '{end}' ORDER BY datum"
    else:
        sql = f"SELECT datum,testy_kumul FROM testy WHERE datum BETWEEN '{start}' AND '{end}' ORDER BY datum"
    return sql, "Počet testů"


@plotTimeSeries
def diedPeople(ax, conn, end, start="2020-03-01", cummulative=False, color="purple"):
    """
    Plots the number of died people against date for specified period
    :param ax: a subplot for plotting the time series into (AxesSubplot object)
    :param conn: connection to the PostgreSQL DB (connection object)
    :param end: last date to plot (str in fmt YYYY-MM-DD)
    :param start: first date to plot (str in fmt YYYY-MM-DD), default 2020-03-01, i.e. first covid case in CZE
    :param cummulative: flag if cummulative values should be plotted (bool), default False
    :param color: line color in the plot (str), default "tomato"
    :return: sql statement and default y label (tuple of two strings)
    """
    if cummulative:
        sql = f"SELECT datum,zemreli FROM souhrn WHERE datum BETWEEN '{start}' AND '{end}' ORDER BY datum"
    else:
        sql = f"""SELECT d.datum,zemreli-zemreli_vcera AS zemreli FROM souhrn AS d JOIN
              (SELECT datum, zemreli AS zemreli_vcera FROM souhrn) AS v ON d.datum = v.datum + INTERVAL '1 DAY'
              WHERE d.datum BETWEEN '{start}' AND '{end}' ORDER BY datum"""
    return sql, "Počet zemřelých"


@plotTimeSeries
def healedPeople(ax, conn, end, start="2020-03-01", cummulative=False, color="green"):
    """
    Plots the number of healed people against date for specified period
    :param ax: a subplot for plotting the time series into (AxesSubplot object)
    :param conn: connection to the PostgreSQL DB (connection object)
    :param end: last date to plot (str in fmt YYYY-MM-DD)
    :param start: first date to plot (str in fmt YYYY-MM-DD), default 2020-03-01, i.e. first covid case in CZE
    :param cummulative: flag if cummulative values should be plotted (bool), default False
    :param color: line color in the plot (str), default "tomato"
    :return: sql statement and default y label (tuple of two strings)
    """
    if cummulative:
        sql = f"SELECT datum,vyleceni FROM souhrn WHERE datum BETWEEN '{start}' AND '{end}' ORDER BY datum"
    else:
        sql = f"""SELECT d.datum,vyleceni-vyleceni_vcera AS vyleceni FROM souhrn AS d JOIN
              (SELECT datum, vyleceni AS vyleceni_vcera FROM souhrn) AS v ON d.datum = v.datum + INTERVAL '1 DAY'
              WHERE d.datum BETWEEN '{start}' AND '{end}' ORDER BY datum"""
    return sql, "Počet uzdravených"


def plotComposition(conn, end, start="2020-03-01", cummulative=False, region=None):
    """
    Plots composition of 2 maps of specidied region and 4 time series of covid evolution between specified dates
    :param conn: connection to the PostgreSQL DB (connection object)
    :param end: last date to plot (str in fmt YYYY-MM-DD)
    :param start: first date to plot (str in fmt YYYY-MM-DD), default 2020-03-01, i.e. first covid case in CZE
    :param cummulative: flag if cummulative values should be plotted (bool), default False
    :param region: NUTS3 code of the desired region (str), default is None, i.e. plotting whole Czech republic
    """
    fig = plt.figure(constrained_layout=True, figsize=[11.7, 8.3])   # create A4 layout
    gs = GridSpec(4, 3, figure=fig)      # divide the layout into regular grid
    # create six subplots
    ax1 = fig.add_subplot(gs[:2, :2])
    ax2 = fig.add_subplot(gs[2:, :2])
    ax3 = fig.add_subplot(gs[0, 2])
    ax4 = fig.add_subplot(gs[1, 2])
    ax5 = fig.add_subplot(gs[2, 2])
    ax6 = fig.add_subplot(gs[3, 2])
    # and feed them with graphics
    plotPrevalentionMap(ax1, conn, end, region=region)
    plotChangeMap(ax2, conn, end, start, region=region)
    infectedPeople(ax3, conn, end, start, cummulative=cummulative)
    performedTests(ax4, conn, end, start, cummulative=cummulative, color="yellow")
    healedPeople(ax5, conn, end, start, cummulative=cummulative, color="purple")
    diedPeople(ax6, conn, end, start, cummulative=cummulative, color="green")

    print("Exporting the composition as a PNG image.")
    fig.savefig("map-composition.png", dpi=600)   # save the figure as PNG image

